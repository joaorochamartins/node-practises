'use strict';

const DB = require('@sysnovare/sys-node-db');

class ClientsDB {

    static async getClients() {

        const serviceDB    = new DB.ServiceDB( 'fiscalizacao');
        let ret = [];
        const clientesSql = {
            sql: ['select id as "id",codigo, nome from fw_clientes']
        };
        ret = await serviceDB.getJson( clientesSql);

        return ret;
    }

}

module.exports = ClientsDB;

'use strict';

const DB = require('@sysnovare/sys-node-db');

class UtilizadoresDB {

    static async get() {

        const utilizadores = [];

        const serviceDB = new DB.ServiceDB('fiscalizacao');
        const utilizadoresDB = await serviceDB.getJson(['select * from fw_utilizadores']);

        for (const utilizadorDB of utilizadoresDB) {
            if (!utilizadorDB.perfis) {
                continue;
            }

            const utilizador = {};
            utilizador.id = utilizadorDB.id;
            utilizador.client = utilizadorDB.cliente;
            utilizador.perf_lang = utilizadorDB.lang;
            utilizador.codigo =  utilizadorDB.codigo;
            utilizador.login =  utilizadorDB.login;
            utilizador.iniciais =  utilizadorDB.iniciais;
            utilizador.nome =  utilizadorDB.nome;
            utilizador.email =  utilizadorDB.email;
            utilizador.foto =  utilizadorDB.foto;
            utilizador.password =  utilizadorDB.password;
            utilizador.perfis =  utilizadorDB.perfis.split(',');
            utilizador.ativo =  JSON.parse(utilizadorDB.ativo);
            // Adiciona ao array
            utilizadores.push(utilizador);

        }

        return utilizadores;
    }

}

module.exports = UtilizadoresDB;

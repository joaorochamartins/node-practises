'use strict';

const Auth = require('../objects.js');
const Sessoes = require('../objects/sessoes.js');
const Utilizador = require('../objects/utilizador.js');
const Utilizadores = require('../objects/utilizadores.js');
const Clients = require('../objects/clients.js');
const AuthMapper = require('../mappers/authMapper');


class AuthService {

    static async getUserAuthentication(request) {

        let utilizador;
        let ret;

        try {
            utilizador = request.auth.credentials.utilizador;
            ret = utilizador.print();
            ret.jwt = request.auth.token;
        }
        catch (error) {
            ret = error;
        }

        return AuthMapper.simpleUtilPrintResponse(ret);
    }

    static async postUserAuthentication(request) {
        let utilizador = Utilizadores.getByClientNLogin(Clients.getClientByCode(request.params.client).id, request.payload.user);
        let ret;

        if (utilizador) {
            utilizador = new Utilizador(utilizador);
            utilizador = await Auth.login(utilizador, request.payload.password);

            if (utilizador) {
                const sessao = await Sessoes.set(utilizador);
                ret = utilizador.print();
                ret.jwt = sessao.id;

                //Falta obter scopes dos utilizadores

                return AuthMapper.simpleUtilPrintResponse(ret);
            }

            return AuthMapper.messageOutputUtilResponse('', -1, 'Senha incorrecta');

        }

        return AuthMapper.messageOutputUtilResponse('', -1, 'Utilizador não existe');
    }

    static async logout(request) {

        let ret;
        try {

            await Sessoes.del(request.auth.token);
            ret = { 'userId': -1, 'userCode': -1 };
        }
        catch( error) {
            ret = error;
        }

        return AuthMapper.simpleUtilPrintResponse(ret);
    }

}



module.exports = AuthService;
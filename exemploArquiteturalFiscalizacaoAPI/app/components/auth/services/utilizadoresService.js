'use strict';

const UtilizadoresMapper = require('../mappers/utilizadoresMapper');
const Utilizadores = require('../objects/utilizadores.js');

class UtilizadoresService {


    static async addUtilizador(request) {

        let output;

        try {
            const item = Object.assign(request.payload.utilizador, { client: request.app.cliente.id });
            const utilizador = await Utilizadores.set(item);
            return UtilizadoresMapper.messageOutputUtilResponse(utilizador.id, 1, utilizador);
        }
        catch (error) {
            output = error;
        }

        return UtilizadoresMapper.simpleUtilPrintResponse(output);
    }
}



module.exports = UtilizadoresService;
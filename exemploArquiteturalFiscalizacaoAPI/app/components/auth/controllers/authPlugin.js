'use strict';

const Joi = require('@hapi/joi');
const AuthService = require('../services/authService');

const tags = ['api', 'auth'];

const validateSession = async function (decoded, request) {

    //Validar que se trata de uma sessão válida
    const sessao = await Sessoes.getById(request.auth.token);
    //Vou validar que o cliente do request é igual ao cliente do token
    if (!sessao.id || decoded.client !== request.app.cliente.id ) {
        return { isValid: false };
    }

    //Sessão válida e retornar credenciais do utilizador
    return { isValid: true, credentials: { utilizador: sessao.utilizador, scope: await sessao.utilizador.getFuncionalidades() } };

};

const myPlugin = {
    name: 'sys-auth',
    version: '1.0.0',
    register: async function (server ) {

        // Register Plugins
        await server.register({
            plugin: require('hapi-auth-jwt2')
        });

        await server.auth.strategy('jwt', 'jwt', {
            key: Util.ConfigJson.jwt.privateKey,
            validate: validateSession,
            verifyOptions: { algorithms: ['HS256'] }
        });

        await server.auth.default('jwt');

        await server.register({
            plugin: require('./authzPlugin')
        });

        await server.register({
            plugin: require('./clientsPlugin')
        });

        await server.register({
            plugin: require('./perfisPlugin')
        });

        await server.register({
            plugin: require('./utilizadoresPlugin')
        });

        //Route to get user
        server.route({
            method: 'GET',
            path: '/auth/{client}/{source}/login',
            options: {
                description: 'User Authentication',
                notes: 'Returns User Data.',
                tags,
                validate: {
                    params: {
                        client: Joi.string()
                            .required()
                            .description('App owner'),
                        source: Joi.string()
                            .required()
                            .description('Request Source (web/app/ext)')
                    }
                }
            },
            handler: function (request) {

                const response = AuthService.getUserAuthentication(request);

                return response;
            }
        });

        // Route for user auth
        server.route({
            method: 'POST',
            path: '/auth/{client}/{source}/login',
            options: {
                description: 'User Authentication',
                notes: 'Returns token.',
                tags,
                validate: {
                    params: {
                        client: Joi.string()
                            .required()
                            .description('App owner'),
                        source: Joi.string()
                            .required()
                            .description('Request Source (web/app/ext)')
                    },
                    payload: {
                        user: Joi.string()
                            .required()
                            .description('User login'),
                        password: Joi.string()
                            .required()
                            .description('User password')
                    }
                },
                auth: false
            },
            handler: async function (request) {

                const response = AuthService.postUserAuthentication(request);

                return response;

            }
        });

        // Logout Route
        server.route({
            method: 'POST',
            path: '/auth/{client}/{source}/logout',
            options: {
                description: 'User Logout',
                notes: 'Remove session information.',
                tags,
                validate: {
                    params: {
                        client: Joi.string()
                            .required()
                            .description('App owner'),
                        source: Joi.string()
                            .required()
                            .description('Request Source (web/app/ext)')
                    }
                }
            },
            handler: async function (request) {

                const response = AuthService.logout(request);

                return response;
            }
        });
    }
};

module.exports = myPlugin;

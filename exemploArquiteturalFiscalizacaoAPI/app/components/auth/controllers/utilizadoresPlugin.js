'use strict';

const UtilizadoresService = require('../services/utilizadoresService');


const myPlugin = {
    name: 'app-auth-utilizadores',
    version: '1.0.0',
    register: async function (server) {

        Funcionalidades.set('UTILIZADORES_VISUALIZACAO', 'Visualização de Utilizadores');
        Funcionalidades.set('UTILIZADORES_ADM', 'Administração de Utilizadores');

        await Utilizadores.init(await UtilizadoresDB.get());


        // apenas se usou a rota de post para exemplificação

        server.route({
            method: 'POST',
            path: '/auth/{client}/{source}/utilizadores/value',
            options: {
                description: 'Grava os valores do Utilizador',
                notes: 'Grava os valores do Utilizador',
                tags: ['api'],
                validate: {
                    params: UtilizadoresJoi.getParamsJoi(),
                    payload: UtilizadoresJoi.getPayloadJoi()
                },
                auth: {
                    scope: 'UTILIZADORES_ADM'
                }
            },
            handler: async function (request) {



                return UtilizadoresService.addUtilizador(request);
            }
        });


    }
};
module.exports = myPlugin;

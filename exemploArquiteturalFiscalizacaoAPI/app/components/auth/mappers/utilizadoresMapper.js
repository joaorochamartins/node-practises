'use strict';

const Joi = require('@hapi/joi');
const CoreJoi = require('../../util/coreJoi');
const UtilOutput = require('./utilOutput');

const payloadJoin = {
    utilizador:
        Joi.object()
            .keys({
                id: Joi.string()
                    .allow('')
                    .description('id do utilizador'),
                codigo: Joi.string()
                    .required()
                    .description('código do utilizador'),
                nome: Joi.string()
                    .required()
                    .description('nome do utilizador'),
                password: Joi.string()
                    .allow('')
                    .description('password do utilizador'),
                email: Joi.string()
                    .required()
                    .description('email do utilizador'),
                login: Joi.string()
                    .required()
                    .description('Login do utilizador'),
                ativo: Joi.boolean()
                    .required()
                    .description('Se o utilizador está ativo'),
                foto: Joi.string()
                    .allow('')
                    .description('foto do utilizador'),
                perfis: Joi.array()
                    .allow('')
                    .description('ids de perfis')
            })

};


const paramsJoi = Object.assign({}, CoreJoi.getParamsJoi());

class UtilizadoresMapper {

    static getPayloadJoi() {

        return payloadJoin;
    }

    static getParamsJoi() {

        return paramsJoi;
    }

    static async simpleUtilPrintResponse(text) {

        return UtilOutput.simpleUtilPrintResponse;
    }

    static async messageOutputUtilResponse(text, value, message) {

        return UtilOutput.messageOutputUtilResponse(text, value, message);
    }
}


module.exports = UtilizadoresMapper;

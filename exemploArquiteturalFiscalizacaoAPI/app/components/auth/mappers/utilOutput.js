'use strict';

const Util = require('@sysnovare/sys-node-utils');

class UtilOutput {


    static async simpleUtilPrintResponse(text) {

        return Util.Output.print(text);
    }

    static async messageOutputUtilResponse(text, value, message) {

        const output = new Util.Output(text, value);
        output.addMessage(message);

        return output;
    }
}



module.exports = UtilOutput;
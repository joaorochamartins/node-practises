'use strict';

const UtilOutput = require('./utilOutput');

class AuthMapper {


    static async simpleUtilPrintResponse(text) {

        return UtilOutput.simpleUtilPrintResponse;
    }

    static async messageOutputUtilResponse(text, value, message) {

        return UtilOutput.messageOutputUtilResponse(text, value, message);
    }
}



module.exports = AuthMapper;
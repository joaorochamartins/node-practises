'use strict';

const DB = require('@sysnovare/sys-node-db');
const Perfis = require('../perfis/perfis');
const Bcrypt = require('bcryptjs');
const Utilizadores = require( './utilizadores' );
const Util = require('@sysnovare/sys-node-utils');

class Utilizador {
    constructor(data = undefined) {

        this.data = {};
        this.data.id = data ? data.id || '' : '';
        this.data.client = data ? data.client || data.cliente || '' : '';
        this.data.perf_lang = data ? data.lang || '' : '';
        this.data.codigo = data ? data.codigo || '' : '';
        this.data.login = data ? data.login || '' : '';
        this.data.iniciais = data ? data.iniciais || '' : '';
        this.data.nome = data ? data.nome || '' : '';
        this.data.email = data ? data.email || '' : '';
        this.data.telefone = data ? data.telefone || '' : '';
        this.data.cor = data ? data.cor || '' : '';
        this.data.foto = data ? data.foto || '' : '';
        this.data.password = data ? data.password || '' : '';
        this.data.perfis = data ? data.perfis || [] : [];
        this.data.ativo = data ? data.ativo : true;
    }

    toDB() {

        const ret = {};
        ret.id = this.data.id;
        ret.cliente = this.data.client;
        ret.codigo = this.data.codigo;
        ret.login = this.data.login;
        ret.email = this.data.email;
        ret.lang = this.data.perf_lang;
        ret.iniciais = this.data.iniciais;
        ret.nome = this.data.nome;
        ret.ativo = this.data.ativo.toString();
        ret.password = this.data.password;
        ret.foto = this.data.foto;
        ret.perfis = this.data.perfis.toString();

        return ret;
    }
    /**
     *
     * @returns {Array}
     */
    async getFuncionalidades() {

        const funcs = [];
        return await Perfis.getPerfilFuncs(funcs, this.data.perfis);
    }

    async set() {

        const serviceDB = new DB.ServiceDB('fiscalizacao');
        const setJson = { table: 'FW_UTILIZADORES', pk: 'id' };

        setJson.data = this.toDB();

        try {
            await serviceDB.setJson(setJson);
        }
        catch (error) {
            Util.Logger.error(error);
        }

        Utilizadores.set(this.data);

        return this.data;
    }

    async del() {

        const serviceDB = new DB.ServiceDB('fiscalizacao');
        const delJson = { table: 'FW_UTILIZADORES', pk: 'id', data: {} };
        delJson.data.id = this.data.id; //this.toDB();
        await serviceDB.del(delJson);
        Utilizadores.del(this.data.id);
        return this.data.id;
    }

    print() {

        const obj = {};
        obj.userId = this.data.id;
        obj.userCode = this.data.codigo;
        obj.loginName = this.data.nome;
        obj.photoUrl = this.data.foto;
        obj.inst_sigla = this.data.client;
        obj.changePass = 0;
        obj.lang = this.data.perf_lang;
        return obj;
    }

    /**
     * Função responsável por fazer um novo set a password do utilizador
     */
    async setPassword() {

        this.data.password = await Bcrypt.hash(this.data.password, 10);
    }

    /**
     * Função vai comparar password do utilizador com password fornecida
     */
    async comparePassword(passwordString) {

        return await Bcrypt.compare(passwordString, this.data.password);
    }
}

module.exports = Utilizador;

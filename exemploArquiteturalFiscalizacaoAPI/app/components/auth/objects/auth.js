'use strict';

class Auth {
    /**
     * Função que permite validar utilizador e password e retornar utilizador em causa
     * @param utilizador
     * @param passwordString
     * @returns {Promise<null|*>}
     */
    static async login( utilizador, passwordString ) {

        let ret;

        try {
            // ret = await utilizador.comparePassword( passwordString );
            ret = true;
        }
        catch ( e ) {
            ret = false;
        }

        // no caso de sucesso retorno o próprio utilizador
        // em caso negativo retorno null
        if ( ret ) {
            return utilizador;
        }

        return null;


    }

}

module.exports = Auth;

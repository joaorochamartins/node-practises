'use strict';

const MapUtil = require('../../util/map');

let _clients = [];
//let _clientsMap = {};
let _clientsMapCodigo = {};

class Clients {

    static setClients(clients) {

        _clients = clients;
        // _clientsMap = MapUtil.mapFromArray(_clients, 'id');
        _clientsMapCodigo = MapUtil.mapFromArray(_clients, 'codigo');

    }

    /*static addClient(client) {

        _clients.push(client);
        _clientsMap = MapUtil.mapFromArray(_clients, 'id');
        _clientsMapCodigo = MapUtil.mapFromArray(_clients, 'codigo');
    }*/

    /*static getClient(id) {

        if (_clientsMap[id]) {
            return _clientsMap[id];
        }

        return {};

    }*/

    static getClientByCode(code) {

        if (_clientsMapCodigo[code]) {
            return _clientsMapCodigo[code];
        }

        return {};

    }
}

module.exports = Clients;

'use strict';

const MapUtil    = require( '../../util/map' );

const _utilizadores    = [];
let _utilizadoresMap = {};

class Utilizadores {

    static get() {

        return _utilizadores;
    }

    /**
     *
     * @param id
     * @returns {{}|*}
     */
    static getById( id ) {

        if ( _utilizadoresMap[ id ] ) {
            return _utilizadoresMap[ id ];
        }

        return {};

    }

    /**
     *
     * @param client
     * @returns {{}|*}
     */
    static getByClient( client ) {

        return _utilizadores.filter( (p) => p.client === client && p.ativo === true );
    }

    /**
     *
     * @param client
     * @param code
     * @returns {{}|*}
     */
    static getByClientNCode( client, code ) {

        return _utilizadores.filter( (p) => p.client === client && p.codigo === code )[ 0 ];
    }

    /**
     *
     * @param client
     * @param code
     * @returns {{}|*}
     */
    static getByClientNLogin( client, login ) {

        return _utilizadores.filter( (p) => p.client === client && p.login === login )[ 0 ];
    }

    /**
     *
     * @param items
     * @returns {Promise<void>}
     */
    static init( items ) {

        _utilizadores.push( ...items );
        _utilizadoresMap = MapUtil.mapFromArray( _utilizadores, 'id' );
    }

    /**
     *
     * @param item
     * @returns {Promise<void>}
     */
    static set(data) {

        if ( _utilizadoresMap.hasOwnProperty( data.id ) ) {
            _utilizadores.splice( _utilizadores.findIndex( (p) => p.id === data.id ), 1 );
        }

        _utilizadores.push( data );
        _utilizadoresMap = MapUtil.mapFromArray(_utilizadores, 'id');

        return data;
    }

    /**
     *
     * @returns {Promise<void>}
     * @param id
     */
    static del(deletedId) {

        _utilizadores.splice( _utilizadores.findIndex( (p) => p.id === deletedId ), 1 );
        _utilizadoresMap = MapUtil.mapFromArray( _utilizadores, 'id' );
        return deletedId;
    }

}

module.exports = Utilizadores;

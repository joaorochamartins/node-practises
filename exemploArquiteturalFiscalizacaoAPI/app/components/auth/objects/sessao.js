'use strict';

const DB = require('@sysnovare/sys-node-db');
const JWT = require( 'jsonwebtoken' );
const Util = require('@sysnovare/sys-node-utils');
const Moment = require( 'moment' );

const setJson = {
    table: 'FW_SESSOES',
    pk: 'jwt'
};


class Sessao {
    constructor( utilizador,jwt, dataInicio, dataFim ) {

        if (jwt) {
            this.id = jwt;
        }
        else {
            this.id         = JWT.sign( {
                client: utilizador.data.client,
                name: utilizador.data.nome,
                code: utilizador.data.codigo
            }, Util.ConfigJson.jwt.privateKey, {
                expiresIn: '18h'
            } );
        }

        this.utilizador  = utilizador;
        //Data Inicio
        if (dataInicio) {
            this.dataInicio = dataInicio;
        }
        else {
            this.dataInicio = Moment();
        }

        //Data Fim
        if (dataFim) {
            this.dataFim    = dataFim;
        }
        else {
            this.dataFim    = Moment().add(18,'h');
        }

    }

    toDB() {

        const ret = {};
        ret.jwt = this.id;
        ret.utilizador = this.utilizador.data.id;
        ret.d_inicio = this.dataInicio.toDate();
        ret.d_fim = this.dataFim.toDate();

        return ret;
    }

    verify() {

        if ( JWT.verify( this.id, Util.ConfigJson.jwt.privateKey ) ) {
            return true;
        }

        return false;

    }


    async set() {

        const serviceDB    = new DB.ServiceDB( 'fiscalizacao');

        setJson.data = this.toDB();

        //Guardar dados da sessão na base de dados
        try {
            await serviceDB.setJson(setJson);
        }
        catch (error) {
            Util.Logger.error('Erro ao submeter dados de sessão.');
            Util.Logger.error(error);
        }

        return this;
    }

    async del() {

        const serviceDB    = new DB.ServiceDB( 'fiscalizacao');

        setJson.data = this.toDB();

        const idToDelete = setJson.data.jwt;

        // remove sessao da base de dados
        try {
            await serviceDB.del(setJson);
        }
        catch (error) {
            Util.Logger.error('Erro ao finalizar sessão.');
            Util.Logger.error(error);
            return null;
        }

        return idToDelete;
    }

}

module.exports = Sessao;

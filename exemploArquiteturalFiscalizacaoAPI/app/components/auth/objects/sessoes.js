'use strict';

const DB = require('@sysnovare/sys-node-db');
const MapUtil = require( '../../util/map' );
const Moment = require( 'moment' );
//
const Sessao  = require( './sessao' );
const Utilizadores = require('../utilizadores/utilizadores.js');
const Utilizador = require('../utilizadores/utilizador.js');

//
const _sessoes    = [];
let _sessoesMap = {};
let loaded = false;

class Sessoes {

    static async loadDB() {

        const serviceDB    = new DB.ServiceDB( 'fiscalizacao');
        const sessoesDB = await serviceDB.getJson(['select jwt as "id", utilizador, to_char(d_inicio,\'\'YYYY-MM-DD HH24:MI:SS\'\') data_inicio, to_char(d_fim,\'\'YYYY-MM-DD HH24:MI:SS\'\') data_fim from fw_sessoes where d_fim > sysdate']);

        for (const sessao of sessoesDB) {
            const utilizador = new Utilizador(Utilizadores.getById(sessao.utilizador));
            _sessoes.push( new Sessao(utilizador,sessao.id,Moment(sessao.data_inicio,'YYYY-MM-DD hh:mm:ss'),Moment(sessao.data_fim,'YYYY-MM-DD hh:mm:ss')) );
        }

        _sessoesMap = MapUtil.mapFromArray( _sessoes, 'id' );
        loaded = true;
    }

    static get() {

        return _sessoes;
    }

    /**
     *
     * @param id
     * @returns {{}|*}
     */
    static async getById( id ) {

        if (!loaded) {

            await this.loadDB();
        }

        if ( _sessoesMap[ id ] ) {
            return _sessoesMap[ id ];
        }

        return {};

    }

    /**
     *
     * @param item
     * @returns {Promise<void>}
     */
    static async set(item) {

        let sessao = new Sessao( item );
        sessao     = await sessao.set();
        if ( _sessoesMap.hasOwnProperty( sessao.id ) ) {
            _sessoes.splice( _sessoes.findIndex( (p) => p.id === sessao.id ), 1 );
        }

        _sessoes.push( sessao );
        _sessoesMap = MapUtil.mapFromArray( _sessoes, 'id' );
        return sessao;
    }

    /**
     *
     * @returns {Promise<void>}
     * @param id
     */
    static async del(id) {

        if (!id || id === undefined) {
            return null;
        }

        let sessao;

        if (!loaded) {

            await this.loadDB();
        }

        if ( _sessoesMap[ id ] ) {
            sessao =  _sessoesMap[ id ];
        }
        else {
            return null;
        }

        const deletedId = await sessao.del();
        _sessoes.splice( _sessoes.findIndex( (p) => p.id === deletedId ), 1 );
        _sessoesMap = MapUtil.mapFromArray( _sessoes, 'id' );
        return deletedId;
    }

}

module.exports = Sessoes;

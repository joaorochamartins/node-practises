# Guia de boas práticas #

Este Documento serve de guia de boas práticas para o desenvolvimento de aplicações em Node.js.

Desta forma, ao longo deste documento são analisados vários aspetos, desde a arquitetura padrão para aplicações node, desde a um conjunto de regras e cuidados que devem ser tomados.

Tendo em conta o enquadramento do desenvolvimento de aplicações node, outros aspetos afetos ao desenvolvimento, planeamento, controlo e qualidade do software são também aqui explicados.

# Arquitetura #

No que concerne à arquitetura, deve ser seguida uma estrutura pré-definida.

As 4 arquiteturas seguintes cobrem diferentes situações genéricas no desenvolvimento de projetos em node e devem ser respeitadas.

## API com Lógica Extensa - grande complexidade ##

Uma aplicação node que sirva de servidor, deve ter uma série de rotas disponíveis, que possam ser utilizadas como pontos de entrada e requisição de diferentes serviços.

De modo a ser possível organizar as diferentes classes por área de negócio, tendo em conta a sua responsabilidade associada, deve-se seguir a seguinte organização.

![Vista_de_Desenvolvimento_nivel2-3domodeloC4](assets/vista_desenvolvimento_2-3.png)

Como é possível ver na figura anterior, um projeto é constituído por vários componentes, desde o código principal - *app* -  a testes, módulos externos, entre outros. Recorrendo com maior detalhe, e a título ilustrativo, à area de negócio "auth", integrante do *app*, cada área de negócio terá uma estrura bem definida, assim como os restantes componentes do projeto. 

O ponto de entrada de cada aplicação deve ser definido no *package.json*, sendo tipicamente um ficheiro com o nome *server.js* ou *index.js* que se encontra na raiz do projeto. Assim como este ficheiro, na raiz do projeto encontram-se outros, como por exemplo *.gitignore*, *gulpfile.js*, o próprio *package.json*, entre outros. 

Dentro da componente *app*, deve existir tipicamente um ficheiro de roteamento principal - *appPlugin* -, que depois guiará os diferentes pedidos para as rotas corretas; uma pasta *Common* para o código comum entre componentes que pode não ser repetido; outras pastas ou ficheiros relevantes; um diretório componentes, onde estarão localizados os vários componentes do Software afetos a diferentes tipos de serviços, funcionalidades ou áreas de negócio.

*Uma nota para quem não esta familiarizado com UML* - É possivel ver umas setas que interligam os componentes dentro da pasta "auth". Estas setas representam as interações padrão entre os diferentes constituintes do componente "auth", representando, se quisermos, e num sentido mais prático, qual sub-componente tem a responsabilidade de invocar/inicializar outro sub-componente. **Esta hierarquia deve ser seguida, salvo certas exceções, de modo a garantir um fluxo de trabalho padronizado que permita uma leitura direta do Software**. Assim, deve ter-se o cuidado de colocar nos devidos (sub)componentes, seguidamente descritos sucintamente, as suas respetivas responsabilidades. 

Relativamente aos 5 (sub)componentes de cada área de negócio, deve-se ainda uma melhor explicação dosa mesmos.

* **Controller** - neste diretório encontram-se as classes que dizem respeito às rotas de cada área de negócio de um projeto, como por exemplo  *authPlugin.js* e *utilizadoresPlugin.js*. As classes deste diretório apenas devem redirecionar pedidos, não devendo ter qualquer lógica de negócio.


* **Services** - neste diretório devem ser colocadas classes com diferentes métodos inerentes a diferentes ações e serviços no contexto do negócio. Aqui é conhecido e manipulado o fluxo de trabalho a ser realizado para uma determinada tarefa/ação/serviço, sendo aqui que deve ser colocada a lógica de negócio de maior nível de cada serviço, e não na camada do *Controller*.

* **Objects** - neste componente devem ser colocados os diferentes objetos relativos ao negócio, ou seja as classes que tem significado a nível de domínio, como por exemplo - e recorrendo novamente ao projeto da *fiscalizacao-api* -  *perfil.js*, *perfis.js*, *sessao.js*, *utilizador.js*, *auth.js*, *ocorrencia.js*, entre muitos outros. Este tipo de classes deve seguir a **programação orientada a objetos**, tendo de se instanciar cada objeto através do seu construtor com base nalguns dados, e depois requerir a execução de determinadas ações através da utilização dos métodos desses mesmos objetos - *dependency injection*. Estes métodos podem ter relativa complexidade, não sendo isso necessariamente um problema, o importante é garantir que certas ações apenas são desempenhadas pelo objeto com o devido direito, sendo ele que tem o conhecimento como determinadas ações devem ser realizadas.

* **Mappers** - normalmente as classes mapeadoras desempenham um papel fundamental na conversão entre diferentes tipos de objetos e na validação da estrutura dos mesmos. Assim, nesta componente devem existir classes com métodos que realizem conversões entre tipos de objetos e métodos que construam o retorno a ser devolvido nas diferentes rotas. Também é nestas classes que se devem definir os diferentes *templates Joi* a serem verificados ou aceites para cada rota.

* **Database** - Este componente deve ser constituído pelas classes que tenham a responsabilidade de atuar sobre a(s) base(s) de dados.


De modo a ilustrar a estrutura agora explicada, adicionou-se a este repositorio um exemplo muito simplificado da aplicação desta estrutura no projeto da fiscalizacao api. Ver [Exemplo] (exemploArquiteturalFiscalizacaoAPI).

***Nota***: Os **Testes devem seguir uma estrutura similar à apresentada na pasta "app"**, no que diz respeito a nomenclaturas e estrutura - ficheiros e diretórios -, de modo a serem manuteníveis de forma mais simples e eficaz.

## Servidor Node com Lógica Reduzida ##

Para situações onde o servidor Node tenha uma envergadura reduzida no que diz respeito à quantidade de código desenvolvido pela Sysnovare, deve-se optar por uma estrutura mais simplificada.

![ArquiteturaServidorSimples](assets/ArquiteturaServidorSimples.png)

Neste caso o ficheiro "appPlugin.js" pode direcionar diretamente as rotas para cada serviço correspondente. Cada serviço pode servir-se de diferentes classes objeto e deve retornar uma resposta correta, recorrendo aos mappers, assim como acontece nos servidores com maior envergadura.

## Utilitário ##

Um projeto que não atue como servidor mas que tenha uma série de funcionalidades utilitárias, como por exemplo uma aplicação cujas funcionalidades são executadas através da linha de comandos, deve ter uma estrutura um pouco diferente.

De forma sucinta, o código lógico deve estar dentro da pasta "app", havendo segregação entre as classes com construtor e comportamento (que devem ser colocadas no diretório "objects") e as classes que apenas contem métodos estáticos (que devem ser colocadas no diretório "services"). É responsabilidade de cada classe objeto a requisição de serviços, sendo que deve existir uma classe dentro da pasta app que redireciona as funcionalidades para os respetivos métodos na classe de objetos.

![Arquitetura serviços](assets/ArquiteturaServicos.png)

Dentro do diretório "Objects" os objetos podem estar agrupados por área, tendo em conta a sua afenidade, caso exista.

Dentro do diretório "Services" os diferentes serviços tem de estar agrupados por responsabilidades - por exemplo realização de pedidos http ou acesso a base de dados.

## Libraria ##

Certo tipo de projetos apenas é utilizado e reutilizado de forma interna, como se de autênticas librarias ou plugins se tratassem. A grande diferença entre este tipo de projetos e os utilitários é que estes visam apenas e só a utilização de recursos node e são utilizados apenas internamente pela Sysnovare, de modo a construir as suas soluções de forma mais eficaz. O que não acontecfe necessáriamente com os utilitários, que podem estar por exemplo instalados em clientes como recurso do sistema para vários projetos.

Este tipo de projetos têm também eles uma estrutura devidamente definida.

![ArquiteturaLibrarias](assets/ArquiteturaLibrarias.png)

# Nomenclaturas #

* Todas as nomenclaturas usadas devem dizer respeito ao negócio e representar as ações efetuadas, de modo a ser possível uma leitura simples do Software. Isto aplica-se tanto ao nome dos diretórios, como nome de classes, dos seus atributos/métodos e todas as variáveis.

* Os nomes das classes, métodos, ficheiros/diretórios e variáveis devem respeitar *CamelCase*. Aqui, a primeira letra deve ser minúscula no nome de ficheiros/diretórios e na instanciação de variáveis simples - que não digam respeito à importação de módulos ou classses.

# Regras de Codificão e Fluxo de trabalho #

* **Em todos os servidores tem de existir uma rota que permita verificar se o servidor está ativo ou não**. Essa rota deve ser: "{{url}}/admin/checkStatus".


```javascript
await server.register({
        plugin: require('hapi-alive'),
        options: {
            path: '/admin/checkStatus', //Health route path
            tags: ['health', 'monitor'],
            auth: false,
            responses: {
                healthy: {
                    message: version
                }
                ,
                unhealthy: {
                    statusCode: 503
                }
            },
            healthCheck: async function () {
            }
        }
});
```

* **Todas as rotas que se pretendam esconder de endereços públicos devem comecar por ".../admin"**, podendo-se assim esconter tudo o que corresponder a estas rotas: "/admin/*".

* Devem ser utilizados métodos assíncronos com *await* em situações onde seja preciso esperar pelo retorno da informação de uma tarefa que não é imediata.

* O **código deve ser comentado**, facilitando a leitura e compreensão do mesmo.

* **Deve-se evitar código repetido.** O código repetido, para além de aumentar desnecessáriamente a envergadura do Software, acarreta outros aspetos negativos, como por exemplo o aumento do trabalho tido para retificar/alterar funcionalidades, tendo em conta que obriga o desenvolvedor a repetir a mesma ação em todos os lugares onde se repetiu um determinado código. Para além disso, pode acontecer o desenvolvedor não ter conhecimento de todas as ocorrencias daquele tipo de código ao longo de todo o Software, não tendo também conhecimento do que poderá acarretar essas modificações em diferentes situações, sendo uma potencial fonte de erros e problemas futuros. Neste tipo de situações, onde exista grande afluencia de código repetido, uma solução poderá ser criar uma(s) classe(s) que trate(m) deste tipo de métodos, sendo essa classe chamada/requisitada sempre que necessário - estas classes devem estar localizadas no diretório *Common*. Em situações onde existam este tipos de classes, mas uma parte dos serviços não se enquadre com a implementação lá percornizada, cabe a estes serviços construir pontualmente a sua própria implementação, idealmente através de um "override". 
É prática da empresa construir [módulos npm comuns](http://tuleap.sysnovare.local:4873/#/) que visam uma maior aproveitação de código e a agilização de tarefas. Em situações onde o desenvolvedor note que um módulo pode ser construído para suprimir certas funcionalidades, deve propor essa solução.

```javascript

// override
var DEFAULT_METHOD = function() {
  return 'new implementaion';
};
```

* Os **métodos** devem ter uma **envergadura reduzida**, partindo-se cada método em vários submétodos caso isso seja necessário.

* O **código** deve estar devidamente **formatado**, de modo a ser percetível imediatamente a estrutura do mesmo.

* O **código deve seguir** indicações do ***Eslint*** - ver mais à frente.

* Devem utilizar-se expressões *try catch* sempre que isso se justifique, por exemplo quando não existe um grande controlo ou verificação da informação passada entre diferentes componentes do sistema. No limite, e para situações onde isto aconteça, os serviços poderão ter este tipo de expressões. No entanto, em situações onde possa haver ambiguidade nos dados, deve sempre haver verificações da integridade da estrutura e conteúdo dos mesmos.

Exemplo de método com try catch:

```javascript
 static async addUtilizador(request) {

    let output;

    try {
        const item = Object.assign(request.payload.utilizador, { client: request.app.cliente.id });
        const utilizador = await Utilizadores.set(item);
        return UtilizadoresMapper.messageOutputUtilResponse(utilizador.id, 1, utilizador);
    }
    catch (error) {
        output = error;
    }

    return UtilizadoresMapper.simpleUtilPrintResponse(output);
}
```

* **Depois de alterações** efetuadas ao Software, **devem ser corridos os testes** do mesmo e adicionar novos testes, em situações onde isso se justifique.

* Deve-se **evitar passar repetidamente objetos completos como parâmetro para diferentes métodos**. Em vez disso, deve-se priveligiar a passagem de atributos concretos que tenham significado e que possam ser imediatamente manipulados pelo método recetor dos mesmos.

* **Configurações de projetos devem ser feitos em ficheiros de config** na raiz do repositório. Para cada ficheiro de configuração, deve haver também um template do mesmo.

* Ao **escolher librarias** deve-se ter em conta os seguintes aspetos: o numero de downloads semanais, data de publicação (ver relacão de data e downloads), o número de projetos dependentes, o número de issues e de pull requests (de modo a ver se tem manutenção ativa). Para além disto, cabe ao desenvolvedor ter um espírito crítico na escolha das librarias.

* Imagens e ficheiros utilitários para o desenvolvimento de código devem ser colocados na pasta "assets". Outros ficheiros deste género, que são por exemplo utilizados para a realização de testes, não necessitam de ser colocados neste diretório.

* Ficheiros que serão utilizados para distribuição de software devem ser colocados na pasta "dist".

* Ações repetidas, como por exemplo testagem, que implicam algumas regras, devem estar incorporadas em scripts dentro do ficheiro package.json.

* **Caso alguma libraria tenha de ser alterada, esta alteração nunca é feita diretamente nos node-modules**. Ao invés disso, deve-se analisar as opções de: realizar um "pull-request" ao repositório da libraria; verificar se extistem outras librarias que implementem as funcionalidades desejadas; desenvolver a funcionalidade em questão no projeto em particular, ou criando uma libraria para esse efeito.

# Eslint - Monitorização de Qualidade #

**Todos os projetos em NodeJs devem ter na sua rais um ficheiro *.eslintr.json***, que agregue um determinado conjunto de regras relativas à estrutura interna de cada classe, garantindo assim uma melhor estrutura, legibilidade e qualidade do software. A utilização deste ficheiro acarreta funcionalidades como a sugestão de boas práticas e aviso de falta de cumrpimento de boas práticas em momento de desenvolvimento e até uma análise integral do código.

Exemplo de Ficheiro:

```json
{
    "env": {
        "commonjs": true,
        "es2020": true,
        "node": true
    },
    "extends": "@hapi/eslint-config-hapi",
    "parserOptions": {
        "ecmaVersion": 11
    },
    "rules": {
        "indent": [
            "error",
            4
        ],
        "linebreak-style": 0,
        "quotes": [
            "error",
            "single"
        ],
        "semi": [
            "error",
            "always"
        ]
    }
}
```

Análise de Código:

> ng lint

# Testes - Monitorização de Qualidade #

Os testes são os dos pontos fundamentais para garantir a qualidade e manutenibilidade de um determinado Software. Assim, para cada produto a desenvolver, deve-se construir um determinado conjunto de testes que assegure estes mesmos aspetos.

Obviamente que assegurar a qualidade de um Software demora tempo, logo custa dinheiro. E até, muitas vezes, determinados clientes até optam por não ter assegurada tanta qualidade no Software em detrimento de terem os seus produtos em posse mais cedo ou até com menos esforço financeiro.

No entanto, a longo prazo isto poderá trazer muitos problemas à empresa desenvolvedora do Software, devido ao aparecimento de erros - e a sua necessária correção-, necessidade de desenvolver novas funcionalidades - o que pode causar problemas em serviços anteriormente desenvolvidos-, entre outros problemas. Assim sendo, o cumprimento de requisitos mínimos para assegurar a confiabilidade do Software é essencial, e parte desses requisitos mínimos reside na realização de testes automáticos.

De modo a testar de forma mais simplificada e rápida o Software construído em node, **os testes serão realizados sobre as rotas, sempre que possível**. Ao serem efetuados testes sobre as rotas, todo o Software das componentes inferiores acaba por ser testado por consequência.

Deve-se ainda ter em atenção que para cada rota existem várias possibilidades de campos de entrada, que levarão potencialmente a um tratamento diferenciado internamente, pelo que essas **diferentes situações devem ser exploradas nos testes**, criando por exemplo um teste para cada situação diferente da mesma rota. Isto possibilita uma maior cobertura do sistema e aumenta o nível de segurança e confiança sobre o mesmo.

Tendo em conta que este tipo de testes agrega as várias componentes do sistema e as suas interações, denominam-se **testes de integração**.

Adicionalmente, e em situações onde seja proveitoso testar determinados métodos individualmente, ou pela sua complexidade ou importância isolada, podem-se criar testes que apenas testem essas situações isoladas (**Testes Unitários**), sendo estes testes aplicados a métodos específicos. No entanto, é importante lembrar que devem-se priveligiar, sempre que possível os testes de integração, porque acabam por testar mais código de uma assentada, sendo por isso mais próximos do comportamento do produto e ficando mais baratos de realizar.

Para a realização dos testes, assim como normalmente acontece em situações de desenvolvimento, deve ser criada uma base de dados específica. Em casos onde exista uma de desenvolvimento, esta pode ser utilizada.

É de realçar ainda que para uma correta e fidedigna execução de testes, a **base de dados a utilizar deve estar previamente com alguma informação apropriada**, podendo e devendo esta ser alterada ao longo da execução dos testes. No entanto, a **execução dos testes não deve comprometer a base de dados em uso** para uma futura e repetida reexecução dos testes.

## Tecnologia de Testes Automáticos ##

Para a realização de testes de integração e testes unitários pode-se utilizar a libraria do **@hapi/lab**.

Esta libraria tem as seguintes particularidades:

* A execução de testes ocorre, no que diz respeito a classes e diretórios, por ordem numérica crescente, sendo por isso responsabilidade do desenvolvedor de atribuir nomenclaturas que permitam a sua utilizacao correta.

* Cada classe de testes tem um *experiment* que será constituído por um ou mais testes. É possivel definir ações a serem realizadas antes dos testes de uma classe, através de um método *before*.

Para mais informações, consultar a [documentação](https://hapi.dev/module/lab/).

## Cobertura de Código ##

O *@hapi/lab* pode fornecer informações acerca da cobertura do código.
Idealmente esta cobertura de código deverá ser igual ou superior ao mínimo definido para cada projeto.

Comando com informações de cobertura:

> lab -c -l -v

Comando com geração de relatório html:

> lab -c -l -v -r html -o coverage.html

[Exemplo de relatório html gerado](/assets/coverage.html)

## Testes de Segurança ##

De forma geral todos os Softwares devem ter mecanismos de segurança.

No entanto, para Softwares que manipulem informações sensíveis, garantir a segurança é um aspeto fundamental.

Assim sendo, e neste tipo de situações, deveriam ser feitos testes à segurança do Softwrare, quer com testes automatizados, como por exemplo *SQL Injection*, ou através de **testes manuais** de *Ethical Hacking*.

# Monitorização de qualidade do Softare #

Obviamente que os testes asseguram, na sua medida, alguma qualidade no que diz respeito ao Software. No entanto, existem aspetos que não são facilmente testáveis com testes, como por exemplo complexidade, otimização, capacidade de resposta e consumo de CPU, pelo que a utilização de algumas ferramentas em paralelo são de grande importancia. Para não falar de por vezes os próprios testes acabarem por ficar aquém do esperado, na medida em que apesar de até poderem ter a cobertura estipulada, não estejam feitos de uma forma rigorosa e que force situações desagradáveis ou impróprias, o que muitas vezes deve ser também testado. Isto acontece fundamentalmente porque os testes são realizados tipicamente pela equipa que esteve envolvida na construção do próprio Software, não tendo dificuldade de construir modelos de teste que respeitem o mesmo.

De seguida serão utilizadas algumas ferramentas que podem ser utilizadas em paralelo para monitorizar outros aspetos. As ferramentas apresentadas não necessitam de ser todas implementadas simultâneamente e em todos os projetos, portanto a sua utilização deve ser ponderada de situação para situação.


## Autocannon ## 

O **Autocannon** é responsável por bombardear o servidor com uma série de pedidos, de modo a avaliar questões de performance e não só. Assim, com o autocannon, algumas questões de desempenho podem ser testadas em ambiente de desenvolvimento. 

Instalar autocannon.

> npm i -g autocannon

O autocannon pode ser executado através de comandos da linha de comandos ou importado no código. Permite a realização de todo o tipo de pedidos, sendo possível definir aspetos como concurrência, número de pedidos, parâmetros de entrada, rotas específicas, entre outros.

[Ver Autocannon](https://www.npmjs.com/package/autocannon)

## Clinic ##

O **Clinic** é uma ferramenta Open-Source, constituída por 3 diferentes tecnologias, cujo objetivo é analisar o desempenho do software.

Instalar o clinic:

> npm i -g clinic


### Clinic Doctor ###

O clinic doctor talvez seja o que dá informação mais relevante, de todos os analisadores do clicic.  Permite rastrear o uso do CPU, ciclos desnecessários ou mal otimizados, identificar situações de quebra entre outros.

É possível correr o Doctor em simultâneo com o *autocannon* e com o arranque da aplicação.

Exemplo:

> clinic doctor --on-port 'autocannon localhost:$PORT' -- npm run start-dev

Isto abre uma janela no navegador com informação relevante:
[![Clinic doctor window](assets/clinic_doctor_exemplo.png)](https://clinicjs.org/assets/videos/doctor-screen-recording.mp4)

[Breve exemplo de Clinic Doctor](https://clinicjs.org/documentation/doctor/).


### Clinic Bubbleprof ###

O clinic Bubbleprof permite avaliar a latência entre as expressões assíncronas do projeto.

Através de uma análise gráfica e textual/numérica podem-se estudar aspetos como tempos médios de resposta e forma de otimizar certos pedidos, reformulando o fluxo de trabalho.

Exemplo:

clinic bubbleprof --on-port 'autocannon -c 5 -a 500 localhost:$PORT' -- npm run start-dev

[![Clinic bubbleproof window](assets/clinic_bubbleproof_exemplo.png)](https://clinicjs.org/assets/videos/bubbleprof-screen-recording.mp4)

[Breve exemplo de Clinic Bubbleprof](https://clinicjs.org/documentation/bubbleprof/).

### Flame ###

O clinic flame permite verificar quais os hotpoints do software, ou seja, as rotas e funcionalidades mais vezes requisitadas. Fornece um navegador gráfico bastante interessante, assim como as restantes funcionalidades do doctor, permitindo uma navegação intuitiva e seguida pelo software e os seus pontos quentes.

A forma de utilização é similar aos anteriores. No entanto este terá mais interesse se aplicado sobre informação de ambiente de produção, sem a utilização do autocannon, pois aqui pretende-se apurar que funcionalidades são mais utilizadas em ambiente real.

[![](assets/clinic_flame_exemplo.png)](https://clinicjs.org/assets/videos/flame-screen-recording.mp4)

[Flame Documentacao](https://clinicjs.org/flame/).


# Controlo e Automatização de Tarefas #

Apesar de todas as recomendações e regras apresentadas,  é importante ter alguma forma relativamente automática de garantir que as métricas apresentadas são cumpridas, dentro de um intervalo de aceitação estipulado. Para além disso, de forma a simplificar os processos por parte do desenvolvdor, algumas tarefas podem ser feitas de forma automática e controlada.

Assim, aspetos como execução, cobertura, entre outros, poderão ser, nalgumas soluções, assegurados através da utilização de Softwares que permitam a definição de Pipelines, como por exemplo o caso do [Jenkins](http://192.105.150.171:8888/login?from=%2F).

O Jenkins é um servidor de automatização de tarefas, que facilita a realização de tarefas como build, testagem, implantação e integração. Para mais informações de como utilizar o Jenkins, verificar [Wiki da Sysnovare - Jenkins](http://wiki.sysnovare.local:8888/development/operations/jenkins).

As pipelines desenvolvidas no Jenkins são normalmente criadas com recurso a um ficheiro "Jenkinsfile", cuja linguagem de programação é o groovy.

Exemplo de estrutura de um Jenkinsfile:

```groovy
#!/usr/bin/env groovy

def differentSORun(cmd) {
    if (isUnix()) {
        sh cmd
    }
    else {
        bat cmd
    }
}

pipeline {
    agent any

    tools {
        nodejs 'node14'
    }

    environment {
        USER_GIT_KEY = '28e97475-e0c6-46fa-a03e-b010744c5dcc'
    }

    stages {

        stage('Git Checkout to Develop') {
            steps {
                git branch:'develop', credentialsId: "${USER_GIT_KEY}", url: 'https://joaorochamartins@bitbucket.org/sysnovare/sys-pdf.git'
            }
        }

        stage('Remove node_modules') {
            steps {
                differentSORun 'rm -rf node_modules'
            }
        }

        stage('Install dependencies') {
            steps {
                differentSORun 'npm set registry http://tuleap.sysnovare.local:4873'
                differentSORun 'npm install'
            }
        }

        stage('Config Setup') {
            steps {
                differentSORun 'cp config-template.json config.json'
            }
        }

        stage('Test') {
            steps {
                differentSORun 'npm run test'
            }
        }

        stage('Build') {
            steps {
                differentSORun 'npm run build'
            }
        }

        stage('Deploy') {
            steps {
                differentSORun 'gulp updateCiServer'
            }
        }
    }

    post {
        failure {
            emailext body: "${currentBuild.currentResult}: Job ${env.JOB_NAME} build ${env.BUILD_NUMBER}\n More info at: ${env.BUILD_URL}",
        recipientProviders: [[$class: 'DevelopersRecipientProvider']],
        subject: "Jenkins Build ${currentBuild.currentResult}: Job ${env.JOB_NAME}",
        to: 'joao.martins@sysnovare.pt'
        }
    }
}

```

# Rastreamento de Bugs #

Muitas das vezes, após realizar uma série de ações num software, existe a perceção de que algo não ocorreu propriamente como esperado.

O que complica esta situação é que muitas vezes os erros que acontecem não bloqueiam a execução do Software (são silenciosos), não havendo por parte do desenvolvedor, por vezes, certezas de onde ocorreu o erro em questão.

Uma forma direta de encontrar origens de erros é a realização do debug, onde o desenvolvedor pode seguir passo a passo a execução do Software, analisar parâmetros de entrada e saída em diferentes métodos e ver todo o comportamento do sistema.

Assim, a utilização de *Debug* para correção de erros, deve ser uma estratégia a adotar.


## Debug com Visual Studio Code ##

O **debug** de projetos nodeJs no **Visual Studio Code** pode ser feito de forma simples. [Ver como fazer debug com VSCode](https://code.visualstudio.com/docs/nodejs/nodejs-debugging).

[Ver Demo de Debug com Visual Studio Code](https://www.youtube.com/watch?v=2oFKNL7vYV8). Video encontra-se também neste repositório dentro da pasta de "assets".


## Debug com node-inspect e Chrome dev-tools ##

O [node-inspect](https://github.com/nodejs/node-inspect) trata-se de uma ferramenta inspetora pode ser utilizado como ferramenta para debug em node.

Instalar globalmente node-inspect: 

> npm install -g node-inspect

Para utilizar este debugger, basta correr a aplicação em causa e depois correr o debugger em paralelo, apontando para o endpoint onde se pretende realizar o debug (um ficheiro em particular) ou para a própria aplicação:


>  node-inspect server.js

>  node-inspect http://localhost:3010

Depois, para um debug interativo, pode-se utilizar o **Chrome dev-tools**, abrindo as *DevTools* (colocar "about:inspect" na barra do Chrome) e selecionando a opção "Open dedicated DevTools for Node". Existem aplicações semelhantes para outros navegadores, nomeadamente Edge - ver [aqui](https://nodejs.org/en/docs/guides/debugging-getting-started/).

Aspeto de debug com o *chrome dev-tools*:

![Debug com chrome dev-tools](assets/dev_tools.png)

No chrome-dev-tools podem ser facilmente manipulados os valores das váriaveis, breakpoints, entre outros.

[Vídeo com demo](https://youtu.be/Xb_0awoShR8).

[Informacao de Video](https://medium.com/@paul_irish/debugging-node-js-nightlies-with-chrome-devtools-7c4a1b95ae27).

*nota*: Usa-se o "node-inspect" ao invés de "node --inspect", devido ao facto de este comando já agrupar uma série de simplificações a adaptações. No entanto, "node --inspect" leva aos mesmos resultados frequentemente.


## Logger ##

Outra abordagem, menos eficaz, passa pela utilização de mecanismos de escrita aquando do momento de execução, como por exemplo o "console.log('...')", "Logger.debug...".

Este mecanismo é utilizado frequentemente, e pode resolver questões simples. No entanto, não fornece ao desenvolvedor a capacidade de monitorizar e controlar em momento de execução o  fluxo lógico e de dados, portanto não será tão eficaz na resolução de problemas.

# Capacidade Deteção de Bugs - Manutenção de Qualidade #

Uma empresa brilha pelas várias qualidades e valores que apresenta. No contexto do desenvolvimento de Software e de soluções informáticas, a garantia de uma maior qualidade e menor percentagem de bugs/erros é um "cartão de visitas" fundamental.

Uma estratégia que pode ser seguida para averiguar a qualidade e aplicabilidade das estruturas de monitorização de qualidade do Software é o rastreamento da capacidade de deteção de bugs ao longo do tempo, ou seja, **em que medida é que a empresa tem a capacidade de detetar erros**.

Uma forma de o fazer é registar a percentagem de bugs encontrados antes de uma release de um produto, face à soma dos mesmos com os que foram apontados pelo cliente. Esta técnica denomina-se ***Defect Removal Efficiency (DRE)*** - eficiência na remoção de defeitos.

Fórmula DRE:  DRE= (numero de defeitos encontrados internamente/ numero de defeitos encontrados internamente + numero de defeitos encontrados externamente) × 100.

Exemplo:

Imaginemos que para uma release, ao longo do processo de desenvolvimento, foram encontrados 7 bugs. Depois, no momento em que se encontra em produção, o cliente, ou os clientes, dependendo do caso, descobriram mais 3 erros. Neste caso a capacidade de deteção de erros foi de 70% ( 7 / 10 x 100 = 70% ) . É natural que esta medida tenha algumas oscilações, mas o aumento desta percentagem ou a sua manutenção pode dar informações muito importantes acerca da qualidade dos produtos vendidos pela Sysnovare.


Esta métrica apenas requer que qualquer bug encontrado, por mais ínfimo que seja, seja colocado num documento, para depois ser feito um balanço.

Os resultados obtidos com este tipo de métricas, especialmente quando positivos, podem e devem ser apresentados pela empresa aos seus clientes, passando uma imagem de qualidade e cuidado.


# Package.json #

Algumas regras a seguir no ficheiro **package.json**.

* Todas as ações repetitivas, que digam por exemplo respeito à build, publicação e testagem do projeto devem estar contidas em **scripts** do package.json.

* As dependencias anunciadas devem referenciar versões especificas dos diferentes módulos a utilizar.

* Muito cuidado com o update dos pacotes de um projeto, pois isso pode levar à geração de erros.

* Deve haver diferenciação entre dependencies e devDependencies, sendo que nas dependencies apenas devem ser colocadas librarias essenciais para o desenrolar do bom funcionamento da aplicação.

Exemplo de Package.json:

```json
{
  "name": "sys-pdf",
  "engines": {
    "node": ">=14.0.0 <15.0.0"
  },
  "version": "1.10.0",
  "description": "PDF utilities for sysnovare projects",
  "main": "server.js",
  "scripts": {
    "test": "lab -v -c test/test.js",
    "start-dev": "gulp prePkg &&  nodemon server.js",
    "build": "gulp prePkg && pkg server.js --config package.json --targets linux,windows --debug --out-path dist > dist/log.txt",
    "deploy-dev": " npm run build && gulp updateDevServer",
    "publish-rc": " npm run build && gulp updateDeployServer",
    "publish": "gulp rcToProd"
  },
  "repository": {
    "type": "git",
    "url": "git+https://antonioCunha@bitbucket.org/sysnovare/sys-pdf.git"
  },
  "author": "Sysnovare",
  "license": "ISC",
  "homepage": "https://bitbucket.org/sysnovare/sys-pdf#readme",
  "dependencies": {
    "@hapi/hapi": "20.1.2",
    "@hapi/inert": "6.0.3",
    "@sysnovare/sys-node-utils": "1.0.2",
    "fs-extra": "9.1.0",
    "get-stream": "6.0.0",
    "hapi-alive": "2.0.4",
    "node-qpdf": "1.0.3",
    "path": "0.12.7",
    "pdf-lib": "1.16.0",
    "simple-node-logger": "18.12.24"
  },
  "devDependencies": {
    "@hapi/code": "8.0.3",
    "@hapi/eslint-config-hapi": "13.0.2",
    "@hapi/eslint-plugin-hapi": "4.3.6",
    "@hapi/lab": "24.1.1",
    "check-engines": "1.5.0",
    "form-data": "4.0.0",
    "gulp": "4.0.2",
    "gulp-ssh": "0.7.0",
    "gulp-zip": "5.1.0",
    "nodemon": "2.0.7",
    "plugin-error": "1.0.1"
  }
}

```

# README #

O Ficheiro **README.md** deve ter informações acerca do projeto em questão e de todos os passos que um novo desenvolvedor terá de realizar para ter o projeto na sua máquina a funcionar e suscétivel a alterações que possam vir a alterar o valor do mesmo.

Assim, normalmente, no readme devem estar presentes informações relativas a; download; instalação; regras e passos para fases de desenvolvimento; testes; build, deploy e publish.

Devem também estar presentes todo o tipo de "dicas" adicionais que possam facilitar a compreensão do novo desenvolvedor, no que diz respeito, por exemplo, à forma como se integrar com o Software.

Exemplo de ficheiro readme:

```
# sys_docx_templates

Projeto para geração de relatórios baseados em templates docx.

## Pré-Requisitos
Git: https://git-scm.com/
Node: https://nodejs.org/en/

Para instalar as dependências:
Necessária ligação vpn.
Ligação ao repositório sysnovare:

> npm set registry http://tuleap.sysnovare.local:4873

Instalar LibreOffice ou Office -> necessário por causa de pdf export.
Caso se instale o Office, o docto.exe deve estar na raiz do projeto.

## Instalação
Fazer clone deste repositório: 

> git clone https://bitbucket.org/sysnovare/sys_docx_templates.git

Entrar no directório do projecto e iniciar o git flow: 

> git flow init

Instalar dependências : 

> npm install

Copiar ficheiro de configuração: 

> cp config-template.json config.json

## Desenvolvimento
Para arrancar com a aplicação: 

> npm run start-dev

Para começar com qualquer alteração ao código: 

> git flow feature start NAME_FEATURE

Para encerrar o desenvolvimento: 

> git flow feature finish NAME_FEATURE

## Testagem

Por vezes é mais fácil testar o modelo com dados locais em vez de url (na maioria das vezes é seguro com auth).
Para tal, colocar um ficheiro no diretorio "sampleData" e usar a tag "file" em vez de "url" -  com a localização do ficheiro - no ficheiro config.json.


## Build & Deploy

Para criar o executável para testes locais: 

> npm run build

Para enviar para o servidor de desenvolvimento Sysnovare: 

> npm run deploy-dev

Para criar release: 

> git flow release start Y.X.Z

Actualizar versão Y.X.Z em package.json

> git add .
> git commit
> git flow release finish Y.X.Z
> git push --all
> git push --tags

Release: 

> npm run publish
```


# index.js / server.js #

Este ficheiro deve ter o mínimo de código possível, tendo como principal responsabilidade servir de ponto de entrada para o sistema em questão.

# hapiServer.js #

Este ficheiro é responsável por registar todas as rotas, recorrendo por exemplo a outros ficheiros que tenham rotas registadas. Aplicável a APIs.

# gulpfile.js #

O ficheiro Gulpfile é utilizado para realização de tarefas de forma automatizada, de modo a evitar ações repetidas.

Normalmente, as ações de build, deploy e release passam todas por tarefas gulp.

Exemplo de ficheiro Gulpfile:

```js'use strict';

const Fs      = require( 'fs' );
const Gulp    = require( 'gulp' );
const GulpSSH = require( 'gulp-ssh' );
const Zip   = require( 'gulp-zip' );
const CheckEngines = require('check-engines');
const PluginError = require('plugin-error');

const json    = JSON.parse( Fs.readFileSync( './package.json' ) );
let env;

const configWeb = {
    host: '192.105.150.165',
    port: 22,
    username: 'webserver',
    password: 'webserver'
};

const configRoot = {
    host: '192.105.150.165',
    port: 22,
    username: 'root',
    password: 'Sysn0var3'
};

const gulpSSHRoot = new GulpSSH( {
    ignoreErrors: false,
    sshConfig: configRoot
} );

const gulpSSHWeb = new GulpSSH( {
    ignoreErrors: false,
    sshConfig: configWeb
} );

const setEnvDev = ( resolve ) => {

    env = 'dev';
    resolve();
};


const serviceStop = () => {

    return gulpSSHRoot.shell( 'service ' + json.name + ' stop' );
};

const serviceStart = () => {

    return gulpSSHRoot.shell( 'service ' + json.name + ' start' );
};

const fileUpdate = () => {

    return Gulp.src( ['./dist/' + json.name + '*'] )
        .pipe( gulpSSHRoot.dest( '/home/node_servers/' + env + '/' + json.name + '/' ) );
};

const filePermission = () => {

    return gulpSSHRoot
        .shell( 'chmod +x /home/node_servers/' + env + '/' + json.name + '/' + json.name + '-linux' );
};

const zipLinuxFile = () => {

    return Gulp.src( ['./dist/' + json.name + '-linux'] )
        .pipe( Zip( json.name + '-linux.zip' ) )
        .pipe( Gulp.dest( 'dist' ) );
};

const zipWinFile = () => {

    return Gulp.src( ['./dist/' + json.name + '-win.exe'] )
        .pipe( Zip( json.name + '-win.zip' ) )
        .pipe( Gulp.dest( 'dist' ) );
};

// ZIP
const zipSend = () => {

    return Gulp.src( ['./dist/*.zip'] )
        .pipe(
            gulpSSHWeb.dest( './node_servers' )
        );
};

const zipVersionSend = () => {

    return Gulp.src( ['./dist/*.zip'] )
        .pipe(
            Zip( '/' + json.name + '/' + json.name + '_' + json.version + '.zip' )
        )
        .pipe(
            gulpSSHWeb.dest( './node_servers' )
        );
};

const checkEngines = async () => {

    CheckEngines((err) => {

        if (err) {
            throw new PluginError('checkEngines', err);
        }

    });

    await Promise.resolve();
};

// This will update our dev server
exports.updateDevServer = Gulp.series( setEnvDev, serviceStop, fileUpdate, filePermission, serviceStart );

// This will update our deploy server
exports.updateDeployServer = Gulp.series( Gulp.parallel( zipLinuxFile, zipWinFile ), zipSend, zipVersionSend );

const createDir = () => {

    return Gulp.src( '*.*', { read: false } )
        .pipe( Gulp.dest( './dist' ) );
};

//So jenkinks can compile and build without error
exports.prePkg = Gulp.series( checkEngines,createDir );

```


# .gitignore #

No ficheiro *.gitignore* devem ser colocados todos os caminhos dos ficheiros que não se pretende colocar no repositório.

# Git #

A Sysnovare priveligia o trabalho através de repositórios Git, recorrendo nomeadamente ao [BitBucket](https://bitbucket.org/dashboard/overview).

* Tendo em conta que a Sysnovare utiliza gitFlow, este deve ser utilizado por todos os desenvolvedores.

* O seguinte comando deve ser sempre realizado quando se realiza um clone de um repositório para a máquina pessoal:

> git flow init

* A ramificação do repositório é constituída por três tipos de ramos: *master*, *develop* e *features*.

* O ramo *master* apenas deve ser utilizado em momentos de entrega do Software.

* O ramo *develop* é utilizado em momentos de desenvolvimento.

* De modo a assegurar a integridade do ramo *develop*, cada funcionalidade a ser realizada por um desenvolvedor deve estar associada a uma *feature* em particular. Apenas no fim da realização dessa funcionalidade, e depois de esta estar devidamente verificada, pode-se passar o código da mesma para o ramo *master*. A finalização de uma *feature* pode não estar ao cargo do desenvolvedor, podendo essa responsabilidade estar atribuída a outro membro da equipa com mais experiência. Este tipo de situação é explicada aquando do momento apropriado.

Para uma breve explicação da estrutura do git flow e do seu funcionamento, consultar os seguintes links:
	-> [git-flow-cheatsheet](https://danielkummer.github.io/git-flow-cheatsheet/).
    -> [git-flow-examplo-modelo](https://riggaroo.dev/wp-content/uploads/2015/09/git-model@2x.png)

* Todo o trabalho realizado e de relevante valor deve ser sempre colocado no repositório, de modo a minimizar estragos em situações onde exista perda de dados na máquina do desenvolvedor.

* As mensagens associadas a cada commit devem ter um título e um sumário - onde o título aborda o tema do commit e o sumário reside numa pequena explicação mais detalhada do conteúdo do commit. O título deve começar com "Title:" e o sumário com "Summary:".

* Para os desenvolvedores pouco acostumados à manipulação de repositórios git através da linha de comandos, algumas ações podem ser facilitadas com a utilização de uma aplicação desktop. Sugestão: [Github Desktop](https://desktop.github.com/).

*  **[Guia da Sysnovare para Git](http://wiki.sysnovare.local:8888/development/tools/git)**

* Para desenvolvedores novos com git: [Ver este link](https://dev.to/milu_franz/git-explained-the-basics-igc).


# Tuleap #

A ferramenta *Tuleap* é utilizada para monitorizar de forma ágil o desenvolvimento realizado, previsto e por realizar associado a cada projeto. Aqui são definidos detalhes relativamente a diferentes serviços/funcionalidades a serem realizadas, para além de ser atribuída a cada tarefa um custo (tempo estimado de resolução)  e uma data limite para a resolução estar finalizada.

O [Tuleap](https://tuleap.sysnovare.local/) deve ser sempre atualizado à medida que as funcionalidades vão sendo realizadas.

# Referências #

* [NodeJS BulletProof](https://dev.to/santypk4/bulletproof-node-js-project-architecture-4epf)

* [Modelo C4](https://c4model.com/)

* [Hapi Lab](https://hapi.dev/module/lab/)

* [Ferramentas de Monitorização para NodeJs](https://sematext.com/blog/nodejs-open-source-monitoring-tools/)

* [NodeJs Monitoring Tools](https://geekflare.com/nodejs-monitoring-tools/)

* [Clinic Tool](https://www.npmjs.com/package/clinic)

* [Ferramentas debug Node](https://nodejs.org/pt-br/docs/guides/debugging-getting-started/)

* [Chrome DevTools Debug](https://medium.com/@paul_irish/debugging-node-js-nightlies-with-chrome-devtools-7c4a1b95ae27)

* Patterns of Enterprise Application Architecture, By Martin Fowler

* A Guide to Selecting Software Measures and Metrics, By Capers Jones

* FLEXIBLE TEST AUTOMATION, By Pasquale Arpaia, Ernesto De Matteis, Vitaliano Inglese

* The Node Beginner Book, By Manuel Kiessling

* The Node Craftsman Book, By Manuel Kiessling

* Hacking The Practical Guide to Become a Hacker, By Jim Kou